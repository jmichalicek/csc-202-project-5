import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Stack;
/*************************
 * Justin Michalicek
 * CSC 202 Spring 2014
 * Project 5
 * CPU Simulation
 * 
 * Reads job data from a text file with 4 whitespace separated values per line
 * <job number> <execution time> <time to enter system> <priority>
 * 
 * Jobs are passed to a simulated processor at the set entry time.
 * The highest priority job is then executed 1 "unit" per cycle until it is complete
 * or a higher priority job is received.  If a job is interrupted to run a higher priority
 * job then it is placed on an active job stack where it waits until higher priority jobs
 * are finished.
 * 
 * Prints the status of each job as it enters the system, begins processing, and completes processing
 * as well as the average amount of time each job spends waiting in the priority queue and the active
 * jobs stack to MichalicekOutput.txt
 *************************/

public class Project5 {
	public static int NEXT_JOB_NUMBER = 1;  // sequential job numbers
	public static int LAST_ENTRY_TIME = 0;  // not sequential but must increase
	public static final String OUTPUT_LOG_FILE = "MichalicekOutput.txt"; 
	public static final String INPUT_FILE_PROMPT = "Enter a file to open or blank to quit: ";
	public static final String PROJECT_HEADING = "Justin Michalicek\nCSC 202 Spring 2014\nProject 5\nCPU Simulation\n";
	
	public static void main(String[] args) {
		PrintWriter logWriter = null;
		try {
			logWriter = new PrintWriter(OUTPUT_LOG_FILE);
		} catch (FileNotFoundException e1) {
			System.out.println("Unable to open file [" + OUTPUT_LOG_FILE + "] for writing.");
			return;
		}
		
		printHeading();
		printHeadingToFile(logWriter);
		logMessage("\n", logWriter);
		
		Scanner jobInputScanner = null;
		ArrayList<Job> jobList = new ArrayList<Job>();
		String inputFileName = null;
		
		Scanner keyboardScanner = new Scanner(System.in);
		if(args.length > 0) {
			inputFileName = args[0];
		} else {
			// priming read
			System.out.print(INPUT_FILE_PROMPT);
			inputFileName = keyboardScanner.nextLine();
		}
		
		// Try to open the input file for reading
		do {
			if(inputFileName.trim().equals("")) {
				logWriter.close();
				return;
			}
			try {
				jobInputScanner = new Scanner(new File(inputFileName));
				break;
			} catch (FileNotFoundException e) {
				System.out.print("Could not open specified input file. ");
				System.out.print(INPUT_FILE_PROMPT);
				inputFileName = keyboardScanner.nextLine();
				inputFileName = inputFileName.trim();
			}
		} while(jobInputScanner == null);
		
		// Try to load the jobs into a list of Job objects, increment counters, etc
		while(jobInputScanner.hasNextLine()) {
			String line = jobInputScanner.nextLine().trim();
			try {
				jobList.add(parseEntry(line));
			} catch (IllegalArgumentException e) {
				logMessage(String.format("%s on line [%s]\n", e.getMessage(), line),
						logWriter);
				continue;
			} catch (NoSuchElementException e) {
				logMessage(String.format("Invalid data on line [%s]\n", line),
						logWriter);
				continue;
			} finally {
				NEXT_JOB_NUMBER++;
			}
		}
		
		logMessage("\n", logWriter);
		runSimulation(jobList, logWriter);
		logWriter.close();
	}
	
	/**
	 * Run the simulation loop.
	 * Runs until the jobList is empty, the simulated CPU is not actively processing a job, there are no jobs on the active stack,
	 * and there are no jobs in the priority queue
	 * 
	 * @param jobList an ArrayList of Job objects
	 */
	private static void runSimulation(ArrayList<Job> jobList, PrintWriter logWriter) {
		PriorityQueue<Job> waiting = new PriorityQueue<Job>();
		Stack<Job> active = new Stack<Job>();
		long waitingTotalWaitTime = 0;
		long activeTotalWaitTime = 0;
		long totalJobCount = 0;
		long currentCycle = 0;
		
		Processor processor = new Processor();
		int nextJobIndex = 0;
		while(nextJobIndex <= jobList.size() - 1 || processor.isCPUActive() || !active.isEmpty() || !waiting.isEmpty()) {
			currentCycle++;
			// if all jobs from the list have not been sent to the CPU then check to see if the next job in the list
			// is supposed to go to the CPU on this cycle
			if(nextJobIndex <= jobList.size() - 1) {
				Job nextJob = jobList.get(nextJobIndex);
				
				if(nextJob.getEntryTime() == currentCycle) {
					waiting.add(nextJob);
					totalJobCount++;
					logMessage(String.format("%d: job  number  %d  (cpu:  %d)  entering the system with a priority of %d\n",
							currentCycle, nextJob.getJobNumber(), 
							nextJob.getExecutionTime(), nextJob.getPriority()),
							logWriter);
					nextJobIndex++;
				}
			}
			
			// If a job is currently processing, interrupt it and move it back to the stack or
			// remove it from the system if it is complete
			if(processor.isCPUActive()) {
				Job currentJob = processor.interrupt();
				if(currentJob.getExecutionTimeRemaining() > 0) {
					active.push(currentJob);
				} else {
					logMessage(String.format("%d: Job number %d finishes processing and leaves the system.\n", 
							currentCycle, currentJob.getJobNumber()),
							logWriter);
				}
			}
			
			Job next = null;
			
			// If there is a job in the priority queue and either no jobs on the active stack
			// or the top of the priority queue is higher priority (so a lower number) than the top of the active stack
			// then grab the top of the priority queue
			if(waiting.peek() != null && 
					(active.isEmpty() || waiting.peek().getPriority() < active.peek().getPriority())) {
				next = waiting.poll();
				waitingTotalWaitTime += next.getWaitTime();
				logMessage(String.format("%d: job  number %d begins processing (cpu: %d)\n",
						currentCycle, next.getJobNumber(), next.getExecutionTime()),
						logWriter);
			} else if(!active.isEmpty()) {
				// if we did not grab the job from the top of the priority queue
				// and there are jobs in the active stack, grab the top one off of the stack
				next = active.pop();
				activeTotalWaitTime += next.getWaitTime();
			}
			
			processor.setCurrentJob(next);
			processor.executeCycle();
			incrementWaitTimes(waiting);
			incrementWaitTimes(active);
		}
		
		logMessage("\n", logWriter);
		logMessage(String.format("Average wait time in input queue: %.1f\n", (double)waitingTotalWaitTime / totalJobCount),
				logWriter);
		logMessage(String.format("Average wait time in active job stack: %.1f\n", (double)activeTotalWaitTime / totalJobCount),
				logWriter);
	}
	
	/**
	 * Increment the wait times for all of the jobs in an Iterable
	 * which will be either the waiting queue or the active stack
	 * 
	 * @param queue
	 */
	private static void incrementWaitTimes(Iterable<Job> jobs) {
		for(Job j: jobs) {
			j.incrementWaitTime();
		}
	}
	
	/**
	 * Log the message to the console
	 * 
	 * @param message
	 */
	private static void logMessage(String message) {
		// makes it easy to change this to printing to a file
		System.out.print(message);
	}
	
	/**
	 * Log the message to a file
	 * 
	 * @param message
	 * @param logWriter
	 */
	private static void logMessage(String message, PrintWriter logWriter) {
		logWriter.write(message);
	}
	
	/**
	 * Parse a single line from the job file and return a Job object
	 * 
	 * @param jobEntry the line from the job file to parse
	 * @return Job object created from the data in the jobEntry
	 * @throws IllegalArgumentException
	 */
	private static Job parseEntry(String jobEntry) throws IllegalArgumentException {
		Scanner entryScanner = new Scanner(jobEntry);
		
		int jobNumber = entryScanner.nextInt();
		if(jobNumber != NEXT_JOB_NUMBER) {
			throw new IllegalArgumentException(String.format("Expected job number %d but got %d", NEXT_JOB_NUMBER, jobNumber));
		}
		
		int executionTime = entryScanner.nextInt();
		int entryTime = entryScanner.nextInt();
		if(entryTime <= LAST_ENTRY_TIME) {
			throw new IllegalArgumentException(String.format("Entry time was %d but must be greater than %d", entryTime, LAST_ENTRY_TIME));
		}
		int priority = entryScanner.nextInt();
		
		Job job = new Job(jobNumber, executionTime, entryTime, priority);
		LAST_ENTRY_TIME = job.getEntryTime();
		
		return job;
	}
	
	private static void printHeading() {
		 System.out.print(PROJECT_HEADING);
	}
	
	private static void printHeadingToFile(PrintWriter logWriter) {
		logWriter.write(PROJECT_HEADING);
	}
}
