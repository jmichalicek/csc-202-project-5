import java.util.List;
import java.util.PriorityQueue;
import java.util.Stack;


public class Processor {

	private Job currentJob;
	
	public Processor() {
		this.currentJob = null;
	}
	
	/**
	 * @return true if there is a job currently active otherwise false
	 */
	public boolean isCPUActive() {
		return this.currentJob != null;
	}
	
	/**
	 * Execute 1 cpu cycle.
	 * Executes 1 cycle of the current job, increments the wait times
	 * for all waiting and active jobs still in the queue and stack,
	 * and increments the current cycle.
	 */
	public void executeCycle() {
		this.runCurrentJob();
	}
	
	/**
	 * Interrupts the currently processing job and returns it
	 * so that it can be completed or put back on the active jobs stack
	 * 
	 * @return the previously current job
	 */
	public Job interrupt() {
		Job c = this.getCurrentJob();
		this.setCurrentJob(null);
		return c;
	}
	
	/**
	 * Runs the currently processing job if there is one
	 */
	private void runCurrentJob() {
		if(this.currentJob == null) {
			return;
		}
		this.currentJob.executeCycle();
	}
	
	/**
	 * @return the currently processing job
	 */
	public Job getCurrentJob() {
		return this.currentJob;
	}
	
	/**
	 * Make a job the actively processing job.  Increment totalActiveWaitTime with how long
	 * the job has been waiting and then reset the job's wait time.
	 * 
	 * @param job
	 */
	public void setCurrentJob(Job job) {
		this.currentJob = job;
	}
	
	@Override
	public String toString() {
		Job currentJob = this.getCurrentJob();
		if(currentJob == null) {
			return "Inactive processor";
		}
		
		return String.format("Processor executing job %d", currentJob.getJobNumber());
	}
}
