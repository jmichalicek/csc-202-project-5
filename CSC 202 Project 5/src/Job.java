/**
 * Class: Job
 * 
 * Represents a job to run on simulated CPU
 * 
 * @author Justin Michalicek  CSC 202 Project 5
 * @version 1.0, updated 3/29/2014
 *
 */

public class Job implements Comparable<Job> {
	public static final short MIN_EXECUTION_TIME = 1;
	public static final short MIN_PRIORITY = 1;
	public static final short MAX_PRIORITY = 4;
	public static final short MIN_JOB_NUMBER = 1;
	public static final short MIN_ENTRY_TIME = 1;

	private int priority; // 1 to 4
	private int jobNumber;
	private int entryTime; // not really a time.  Cycle number the job should enter the system
	private int executionTime; // amount of cycles required to execute the job
	private int waitTime;
	private int executionTimeRemaining;

	public Job(int jobNumber, int executionTime, int entryTime,
			int priority) {
		this.setJobNumber(jobNumber);
		this.setExecutionTime(executionTime);
		this.setEntryTime(entryTime);
		this.setPriority(priority);
		this.setExecutionTimeRemaining(executionTime);
	}

	/**
	 * Return the priority of the job.  1-4 with 1 being highest priority
	 * 
	 * @return the job priority
	 */
	public int getPriority() {
		return this.priority;
	}

	/**
	 * Returns how many cycles a job has been waiting since it was last reset
	 * 
	 * @return the wait time
	 */
	public int getWaitTime() {
		return this.waitTime;
	}

	/**
	 * Reset the time a job has been waiting to 0
	 */
	public void resetWaitTime() {
		this.waitTime = 0;
	}

	/**
	 * Increment how long the job has been waiting
	 */
	public void incrementWaitTime() {
		this.waitTime++;
	}

	/**
	 * Set the job's priority and ensure that it is between MIN_PRIORITY and MAX_PRIORITY inclusive
	 * @param newPriority
	 */
	public void setPriority(int newPriority) {
		if (newPriority < MIN_PRIORITY || newPriority > MAX_PRIORITY) {
			throw new IllegalArgumentException(String.format(
					"Priority must be %d through %d.", MIN_PRIORITY,
					MAX_PRIORITY));
		}

		this.priority = newPriority;
	}

	/**
	 * Return the jobs number
	 * 
	 * @return the job number
	 */
	public int getJobNumber() {
		return this.jobNumber;
	}

	/**
	 * Set the job number
	 * 
	 * @param number
	 */
	public void setJobNumber(int number) {
		if (number < MIN_JOB_NUMBER) {
			throw new IllegalArgumentException(String.format(
					"Job number must be at least %d.", MIN_JOB_NUMBER));
		}
		this.jobNumber = number;
	}

	public int getEntryTime() {
		return this.entryTime;
	}

	/**
	 * Set the time which the Job is allowed to enter the system
	 * 
	 * @param time the job may enter the system
	 */
	public void setEntryTime(int time) {
		if (time < MIN_ENTRY_TIME) {
			throw new IllegalArgumentException(String.format(
					"Entry time must be at least %d.", MIN_ENTRY_TIME));
		}
		this.entryTime = time;
	}

	/**
	 * Return how many cycles it takes to complete the job
	 * 
	 * @return execution time
	 */
	public int getExecutionTime() {
		return this.executionTime;
	}

	/**
	 * Set how many cycles it takes for the job to complete
	 * 
	 * @param time how many cycles it takes to complete the job
	 */
	public void setExecutionTime(int time) {
		if (time < MIN_EXECUTION_TIME) {
			throw new IllegalArgumentException(
					String.format("Execution time must be at least %d.", MIN_EXECUTION_TIME));
		}
		this.executionTime = time;
	}
	
	/**
	 * Returns how many cycles the job still needs to run before it is complete
	 * 
	 * @return execution time remaining
	 */
	public int getExecutionTimeRemaining() {
		return this.executionTimeRemaining;
	}
	
	
	/**
	 * Used internally to set how many cycles the job still needs to run.
	 * 
	 * @param time
	 */
	private void setExecutionTimeRemaining(int time) {
		if(time > this.executionTime) {
			throw new IllegalArgumentException("Execution time remaining cannot be greater than total execution time");
		}
		this.executionTimeRemaining = time;
	}

	/**
	 * Execute the job for 1 cycle
	 */
	public void executeCycle() {
		this.setExecutionTimeRemaining(this.getExecutionTimeRemaining() - 1);
		this.resetWaitTime();
	}

	/**
	 * Compare the Job to another Job object.  Compares based on priority then job number
	 * with smaller numbers coming first.
	 */
	@Override
	public int compareTo(Job o) {
		int p = this.getPriority() - o.getPriority();
		if(p != 0) {
			return p;
		}
		
		return this.getJobNumber() - o.getJobNumber();
	}
	
	/**
	 * Compare this Job to another object for equality.
	 * Jobs are equal if they reference the same object or
	 * if they have the same jobNumber and priority.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == this) {
			return true;
		}
		
		if(obj == null) {
			return false;
		}
		
		if(!(obj instanceof Job)) {
			return false;
		}
		
		Job other = (Job)obj;
		return this.getJobNumber() == other.getJobNumber() && this.getPriority() == other.getPriority();
	}
	
	@Override
	public String toString() {
		return String.format("Job number %d with priority %d", this.getJobNumber(), this.getPriority()); 
	}

}